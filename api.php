<?php
    

	
	require_once("Rest.inc.php");
	
	class API extends REST {
	
		public $data = "";
		
		const DB_SERVER = "182.50.133.90:3306";
		const DB_USER = "matrimonialuser";
		const DB_PASSWORD = "oD5%ou64";
		const DB = "matrimonialdb";
		
		private $db = NULL;
	
		public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->dbConnect();					// Initiate Database connection
		}
		
		/*
		 *  Database connection 
		*/
		private function dbConnect(){
			$this->db = mysql_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD);
			if($this->db)
				mysql_select_db(self::DB,$this->db);
		}
		
		/*
		 * Public method for access api.
		 * This method dynmically call the method based on the query string
		 *
		 */
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404);				// If the method not exist with in this class, response would be "Page not found".
		}
		
		/* 
		 *	Simple login API
		 *  Login must be POST method
		 *  email : <USER EMAIL>
		 *  pwd : <USER PASSWORD>
		 */
		
		private function login(){
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			$userName = $this->_request['username'];		
			$password = $this->_request['password'];
			
			// Input validations
			if(!empty($userName) and !empty($password)){
				
					$sql = mysql_query("SELECT id FROM login WHERE username='$userName' AND password='".$password."'", $this->db);
					if(mysql_num_rows($sql) > 0){
						$result = mysql_fetch_array($sql,MYSQL_ASSOC);
						$data=array("response_code"=>"1","message"=>'Sucessfully loged in!');
						
						// If success everythig is good send header as "OK" and user details
						$this->response($this->json($data), 200);
					}else{
						 $data=array("response_code"=>"0","message"=>'Invalid Username Or Password!');
						 $this->response($this->json($data), 200);
					}
					$this->response('', 204);	// If no records "No Content" status
				
			}
			
			// If invalid inputs "Bad Request" status message and reason
			$error = array('status' => "Failed", "msg" => "Invalid Email address or Password");
			$this->response($this->json($error), 400);
		}

		private function storelocation()
		{
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			
			$userName = $this->_request['username'];
			$lattitude = $this->_request['lattitude'];
			$longitude =$this->_request['longitude'];
					
			$sq="UPDATE login SET lattitude='$lattitude', longitude='$longitude' WHERE username='$userName'";
			$s=mysql_query($sq, $this->db);
			$data=array("response_code"=>"1","message"=>'Sucessfully updated!');
			$this->response($this->json($data), 200);
					
		}

		private function tracking()
		{
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			
			$userName = $this->_request['username'];
			
					
			

			$sql = mysql_query("SELECT username,lattitude,longitude FROM login WHERE username!='$userName'", $this->db);
			if(mysql_num_rows($sql) > 0){
				$result = array();
				while($rlt = mysql_fetch_array($sql,MYSQL_ASSOC)){
					$result[] = $rlt;
				}
				// If success everythig is good send header as "OK" and return list of users in JSON format
				$this->response($this->json($result), 200);
			}
			$this->response('',204);	// If no records "No Content" status
					
		}

		/*
		 *	Encode array into JSON
		*/
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
	}
	
	// Initiiate Library
	
	$api = new API;
	$api->processApi();
?>